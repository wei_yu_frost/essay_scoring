# 英文作文评分实验
自己先后尝试了4个版本去测试，essay_scoring_V5是比较完整的版本，其中包含了：
- 词法特征
- 句法特征
- 语法错误特征（使用了language-check）
- 标点特征
- 老师给的特征（与上面的有重叠）
- 语义特征（word2vec向量100维+聚类特征）
数据集处理：主要是把替换了缩写 比如把I'm 换成了I am等；
得出特征之后，进行特征归一化。
但是最后的效果还是没有明显提升，随机森林模型表现为0.7487018816342377。
后面的改进思路：
1. 爬取收集中国初中生英语作文，针对中国学生初中英语作文训练专门的word2vec（现在使用的是谷歌训练的100维词向量）
2. 我根据打印出来的评分观察比较了一下，觉得比较重要的还是语法检错模块，我测试了language-check，但是觉得检错效果一般，我看到了论文中有这么一种方法,认为有可行之处：
>  _基于现成的 N-gram 字典对作文抽取语法正确性特征。该字典使用英文维基百科语料，统计其中的 3-gram、4-gram、5-gram 词汇组合及其对应的词频。构建作文语法正确性特征的过程如下： a.读取维基百科的 N-gram 数据，将其转化成字典格式。 b.对作文文本进行分句、分词等预处理工作。 c.对作文分词后的词汇集合，分别设定 3、4、5 为窗口大小，得到不同窗口大小下的 n-gram 文本。 d.根据维基百科的 3-gram、4-gram、5-gram 字典，对作文的 n-gram 文本集合进行统计。_ 
3. 探索下来发现，我觉得回归表现比分类表现好，我之前看到的论文都是以ASAP数据集为基础的，ASAP评分其中的4个主题作文评分分档很小（分为1 2 3 4 分），所以分类效果好一些，但是这个数据集分值分档较多，所以我觉得最好是用回归来做。



# 10.1-10.23工作
## 1. 论文阅读

这段时间来自己一直在阅读作文自动评分与简答题自动评分相关的论文，总共阅读了大概40+，大部分为国内硕士论文，现将论文方法整理如下，供之后借鉴：
### 1)简答题自动评分
简答题自动评分主要是计算学生答案与参考答案之间的相似度，我整理了作文中常见的相似度计算方法，而大部分论文的方法都是把这些相似度特征分别乘以各自的权重，进而得到句子或段落的相似度特征；我认为之后可以分析不同科目的简答题特征，融合多种算法并进行定制化改进来完成简答题自动评分。
#### 句子表层特征：
- 词形相似度：（这个方法可以扩展到计算 重叠名词/动词/形容词个数等）

![输入图片说明](https://images.gitee.com/uploads/images/2021/1024/151713_9ba5209c_5736994.png "屏幕截图.png")

- 句长相似度：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1024/151718_8f5f5f02_5736994.png "屏幕截图.png")
- 句序相似度：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1024/151723_96c484c3_5736994.png "屏幕截图.png")

这里的wordonce 表示在A和B中仅出现过一次的词语

 > 张胜楠. 基于文本相似度的短文本主观题自动评分方法研究[D].武汉理工大学,2017._ 

![输入图片说明](https://images.gitee.com/uploads/images/2021/1024/151734_d659038c_5736994.png "屏幕截图.png")


> 
>  _何恒飞. 主观题智能阅卷的关键技术研究[D].北京工业大学,2013._ 

- 模糊数学中的单向贴近度：
定义：设A、B是字符串A中包含n个字符δ（AB）表示A贴近于B的单向贴近度按照从左到右的顺序集合A中的每个元素在集合B中出现的有效次数和记为m则δ（AB）＝m／n；张胜楠提出可以引用同义词词林/设置关键词权重进而改进单向贴近度算法；何恒飞提出可以通过关键词的匹配程度来计算关键词的单向贴近度，然后再求整个句子的单向贴近度；
 

> _孟爱国,卜胜贤,李鹰,甘文.一种网络考试系统中主观题自动评分的算法设计与实现[J].计算机与数字工程,2005(07):147-150.

>  _张胜楠. 基于文本相似度的短文本主观题自动评分方法研究[D].武汉理工大学,2017._ 

>  _何恒飞. 主观题智能阅卷的关键技术研究[D].北京工业大学,2013._ _ 
- 基于字符串计算相似度：
  -  编辑距离
  - 汉明距离 
  - LCS:最长公共子串
  - N-gram: n-gram共现特征：类似于机器翻译评估，计算unigram/bigram/trigram特征；赵丹提出可以通过词性过滤（只计算实词）、题干过滤（删除学生摘抄的题干中的词汇）来改进N-gram共现算法；
  - Jaccard
  - Overlap Coeeficient


>  _杨靖云. 高考历史简答题自动评价方法研究[D].哈尔滨工业大学,2016._ 

>  _赵丹. 主观题自动评分系统的研究与实现[D].西安电子科技大学,2019._ 

#### 语义相似度：
- 词语语义相似度：很多研究者都是基于《知网》义原或《同义词词林》来计算词语的语义相似度。张胜楠考虑到了知网层次体系中的义原密度，曹建奇使用《同义词词林》作为《知网义原的补充》；张添一设计了如何通过词语相似度计算出句子和段落的相似度（词语相似度矩阵取最大值后删除最大值所在的行和列，得到最大值序列后取平均即为句子相似度；段落相似度计算同理）；何恒飞做了第一义原相似度的计算与改进；柏雪将句子划分为主要功能模块，然后计算每一模块中的词语语义相似度（基于《知网》），然后根据不同功能模块的权重得到句子的整体相似度；孙润志认为按照主谓宾划分是没有意义的，他认为应该按照句子中实词的语句相似度计算；
 _

> 曹建奇. 基于自然语言处理的主观题自动评分系统的研究与实现[D].北京工业    大学,2015.

> 张胜楠. 基于文本相似度的短文本主观题自动评分方法研究[D].武汉理工大学,2017.

> 丁振国,陈海霞.一种基于知网的主观题阅卷算法[J].微电子学与计算机,2008(05):108-109+113.

> 张添一. 基于文本相似度计算的主观题自动阅卷技术研究[D].东北师范大学,2011.

> 何恒飞. 主观题智能阅卷的关键技术研究[D].北京工业大学,2013.

> 柏雪. 主观题自动阅卷系统的研究与设计[D].西南交通大学,2013.

> 孙润志. 基于语义理解的文本相似度计算研究与实现[D].中国科学院研究生院（沈阳计算技术研究所）,2015.

> 金博,史彦军,滕弘飞.基于语义理解的文本相似度算法[J].大连理工大学学报,2005(02):291-297.

- 基于向量空间模型：主要可以使用one-hot,tf-idf,wordvec等表示句子，然后使用夹角余弦值来做计算两个句子之间的相似度。赵丹提出可以通给予实词和虚词不同的权重来改进文本向量；
> 杨靖云. 高考历史简答题自动评价方法研究[D].哈尔滨工业大学,2016.

> 赵丹. 主观题自动评分系统的研究与实现[D].西安电子科技大学,2019.

> 蔡玮,黄陈蓉,林忠,韩磊.一种基于向量空间模型的主观题批改算法[J].计算机与现代化,2008(12):88-90.

> 韩磊,黄陈蓉,林忠,蔡玮.简答题在线自动批改系统的研究[J].南京工程学院学报(自然科学版),2008,6(03):34-38.
#### 句法分析 
曹建奇提出使用工具（如LTP或standfordnlp等）获取句子的句法结构信息，包括主谓宾定状补、主谓关系、动宾关系、定中关系、状中关系，动补关系，将五种关系在句子中出现的次数构建向量，然后使用夹角余弦值计算句子关系之间的相似度。
> 曹建奇. 基于自然语言处理的主观题自动评分系统的研究与实现[D].北京工业大学,2015.

提取句子主干部分（核心谓语/与核心谓语有直接依存关系的动名词），然后根据依存关系进行相似度计算。
> 赵丹. 主观题自动评分系统的研究与实现[D].西安电子科技大学,2019.
#### 基于知识图谱（知识点结构树）的匹配算法：
主要是根据学生答案对应的节点与标准答案对应节点之间的关系来计算两者之间的匹配度
> 张添一. 基于文本相似度计算的主观题自动阅卷技术研究[D].东北师范大学,2011.

### 2)作文自动评分
#### 特征抽取：
##### 1. 文本表层特征
> 何屹松,孙媛媛,汪张龙,竺博.人工智能评测技术在大规模中英文作文阅卷中的应用探索[J].中国考试,2018(06):63-71.

- 结构特征：单词数量、位置特征、标点符号特征、文章长度、词汇平均长度、去停用词后词表大小、词表大小占作文长度的比例
- 词汇特征：n-gram（词汇丰富度），指示词、人称代词、情态动词、专有名词、pos标注后的人称代词(PRP)/专有名词(NNP)/情态动词(MD)、动词、名词、形容词、介词所占比例，正确的词性标签组合特征（效果不好）、拼写错误单词个数（pycorrector）、连接词个数（所有连接词发生率、因果连接词发生率、逻辑连接词发生率、对比转折连接词发生率、时间连接词发生率、扩展连接词发生率、添加连接词发生率、肯定连接词发生率和否定连接词发生率、音节数（可读性）、段落数；单词的MTLD系数、VOCD系数；彭丽莎和陈志恒都提出可以使用成语/诗词特征/比喻/拟人/优美句/排比个数作为评价中小学语文作文的特征
- 句法特征：句法分析树深度特征、平均句长和方差、长度大于5/10/15/25的句子数量，句子深度最大值、平均值，平均从句数量，平均从句长度，句子平均动词、名词、情态动词、介词、标点数目，陈姗姗提出使用递归自编码基于句法树结构将句子语义表示成较高维度的向量；刘建阳：文本难度、文章的叙事性、句法简易程度、词义丰富度、引用衔接、深度衔接、动词衔接、逻辑关系及时间序列信息等特征；如名词重叠、词根重叠、名词代词重叠、实词重叠和指代重叠
- 语法错误特征：N-gram检查（3-gram/4-gram/5-gram）


> 周明,贾艳明,周彩兰,徐宁.基于篇章结构的英文作文自动评分方法[J].计算机       科学,2019,46(03):234-241.

> 王川. 基于自然语言处理的作文自动评分系统研究[D].武汉理工大学,2015.

> 陈珊珊. 自动作文评分模型及方法研究[D].哈尔滨工业大学,2017.

> 刘建阳.英文作文自动评分算法研究及系统实现[D].北京大学,2014

> 彭丽莎. 中考语文作文评分建议系统的设计与实现[D].华中科技大学,2020.

> 陈志恒. 基于深度学习的中文作文智能评测系统的设计与实现[D].中国科学院大学(中国科学院沈阳计算技术研究所),2021.

- 题目特征:计算作文词汇与题目词汇的重合度（相似度）--->效果不好
陈姗姗提出可以使用主题相关度：文本相似度（重合词汇/重合的动词、名词、形容词等）、基于词向量的相似度（word2vec）、基于篇章向量表示的相似度（LDA/doc2vec）、语义离散度特征（文本中每个句子语义的差异程度）


> 王川. 基于自然语言处理的作文自动评分系统研究[D].武汉理工大学,2015.

> 陈珊珊. 自动作文评分模型及方法研究[D].哈尔滨工业大学,2017.

##### 2. 潜在语义分析
- 单词向量空间：如word2vec/tf-idf/doc2vec，陈姗姗采用了新的词向量组合方式，选择句子中的词向量在每个维度上的最大值和最小值，拼接得到句向量
- 话题空间/主题模型，如LSA/PLSA/LDA


> 何屹松,孙媛媛,汪张龙,竺博.人工智能评测技术在大规模中英文作文阅卷中的应用探索[J].中国考试,2018(06):63-71.

> 梁茂成,文秋芳.国外作文自动评分系统评述及启示[J].外语电化教学,2007(05):18-24.

> 陈珊珊. 自动作文评分模型及方法研究[D].哈尔滨工业大学,2017.

- 词聚类：陈姗姗提出可以通过聚类算法对词向量进行分类，根据词汇对应的类别获取作文篇章在各个类别下的词汇分布情况。
> > 陈珊珊. 自动作文评分模型及方法研究[D].哈尔滨工业大学,2017.

- 情感特征：陈姗姗提出使用斯坦福CoreNLP情感分析引擎提取句子情感，分别采集6 个类别下的情感词数量、句子中包含的情感词数量、积极情感词与消极情感词的比例 ，所属的情感；除此之外，也将句子分为5类：非常消极、消极、中立、积极、非常积极。根据句子所属类别为句子构建一个维度为 5 的特征向量，所属类别对应的值为 1，其余置为 0。
- 篇章特征：可读性（Flesch、 Flesch- Kincaid、Coh- Metrix l2）


> 陈珊珊. 自动作文评分模型及方法研究[D].哈尔滨工业大学,2017.

> 彭丽莎. 中考语文作文评分建议系统的设计与实现[D].华中科技大学,2020.
#### 训练方法：
1. RNN+LSTM提取文章深度网络内容特征，然后送入岭回归；
> 何屹松,孙媛媛,汪张龙,竺博.人工智能评测技术在大规模中英文作文阅卷中的应用探索[J].中国考试,2018(06):63-71.
2. 使用SVM/随机森林/极端梯度上升等对篇章成分进行分类，最后构建线性回归模型进行评分；
> 周明,贾艳明,周彩兰,徐宁.基于篇章结构的英文作文自动评分方法[J].计算机科学,2019,46(03):234-241.
3. 特征融合：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1024/151638_904aadad_5736994.png "屏幕截图.png")

> 陈珊珊. 自动作文评分模型及方法研究[D].哈尔滨工业大学,2017.

由于大部分论文都在使用传统方法，所以深度方法我还没有完全梳理处理，而且自己本身也在学习过程中，所以之后会继续加强深度学习与自动评分结合在一起的学习。

## 2. 自动评分实践部分
主要是使用抽取特征+尝试分类和回归做的自动评分
根据评分标准抽取主要特征，使用斯皮尔曼系数计算相关性，删掉相关性小的一些特征后进行数据归一化，分别尝试线性回归、岭回归、随机森林、xgoost、gbdt等,结果最好的是xgboost，准确率73%，猜测特征数过少，而且在计算斯皮尔曼系数时，没有特征与标签的相关性达到60%+，所以特征代表度不够；

### 主要特征
- 词向量
- 平均单词长度
- 文章单词总数
- 文章字母总数
- 句子总数
- 词汇丰富度
- 错误单词总数
- 名词/形容词/副词/连词数量

## 3. OCR训练
主要完成了数据集抽取、分类、分析，PPOCR的数据label生成等工作，代码已经放在服务器端了；